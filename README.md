# WebsiteHerbig - Hugo theme
A theme for hugo with a markdown-ish UI.

## Live Website

Check out my [Website](https://herbig-burbach.de) to get an impression. [On codeberg.org](https://codeberg.org/muro/WebsiteHerbig) you find the source code for the website.

## Feature
- Callouts
- Tags
- tl;dr

## Installation
In your Hugo website directory, create a new folder named theme and clone the repo
```bash
$ mkdir themes
$ cd themes
$ git clone https://codeberg.org/muro/WebsiteHerbigHugoTheme.git
```
Edit the `config.toml` file with `theme="WebsiteHerbigHugoThese"`
For more information read the official [setup guide](https://gohugo.io/overview/installing/) of Hugo.

## Writing Posts
Create a new `.md` file in the *content/posts* folder
```yml
---
title: Title of the post
description:
date:
tldr: (optional)
draft: true/false (optional)
tags: [tag names] (optional)
---
```

## Credits
**Forked** from [Archie Theme](https://github.com/athul/archie) and Licensed under MIT License

**Icons** from [simpleicons.org](https://simpleicons.org/)

**Fonts** from [design.ubuntu.com](https://design.ubuntu.com/font/)

